package com.pragma.plazoleta.plazoleta_hexagonal.application.handler;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserRequestDto;
import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserResponseDto;
import com.pragma.plazoleta.plazoleta_hexagonal.application.mapper.UserRequestMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.application.mapper.UserResponseMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IRoleServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IUserServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserHandler implements IUserHandler{

    private final IUserServicePort userServicePort;
    private final IRoleServicePort roleServicePort;
    private final UserRequestMapper userRequestMapper;
    private final UserResponseMapper userResponseMapper;

    @Override
    public void saveUser(UserRequestDto userRequestDto) {
    RoleModel roleModel = roleServicePort.getRole(userRequestDto.getRoleId());
    UserModel userModel = userRequestMapper.toUser(userRequestDto);
    userModel.setRole(roleModel);
    userServicePort.saveUser(userModel);
}

    @Override
    public UserResponseDto getUserById(Long userId) {
        UserModel userModel = userServicePort.getUserById(userId);
        return userRequestMapper.toDto(userModel);
    }


    @Override
    public List<UserResponseDto> getAllUser() {
        return userResponseMapper.toResponseList(userServicePort.getAllUser());
    }
}
