package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.adapter;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IRolePersistencePort;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper.IRoleEntityMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IRoleRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RoleJpaAdapter implements IRolePersistencePort {

    private final IRoleRepository roleRepository;
    private final IRoleEntityMapper roleEntityMapper;
    @Override
    public RoleModel getRole(Long roleId) {
        return roleEntityMapper.toRoleModel(roleRepository.findById(roleId).orElseThrow(NoDataFoundException::new));
    }
}
