package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleRepository extends JpaRepository<RoleEntity, Long> {

}
