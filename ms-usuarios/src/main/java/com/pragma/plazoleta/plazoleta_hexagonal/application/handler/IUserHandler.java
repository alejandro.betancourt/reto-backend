package com.pragma.plazoleta.plazoleta_hexagonal.application.handler;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserRequestDto;
import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserResponseDto;

import java.util.List;

public interface IUserHandler {

    void saveUser(UserRequestDto userRequestDto);

    UserResponseDto getUserById(Long userId);

    List<UserResponseDto> getAllUser();
}
