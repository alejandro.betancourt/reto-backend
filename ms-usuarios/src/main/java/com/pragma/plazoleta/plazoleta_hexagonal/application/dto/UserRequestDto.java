package com.pragma.plazoleta.plazoleta_hexagonal.application.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
public class UserRequestDto {

    @NotBlank(message = "Name is required")
    private String name;

    @NotBlank(message = "Last name is required")
    private String lastName;

    @NotNull(message = "Identification document is required")
    @Pattern(regexp = "[\\d]*", message = "Identification Document must be numeric")
    private String identificationDocument;

    @NotBlank(message = "Cellphone is required")
    @Size(max = 13, message = "The cellphone number must contain a maximum of 13 characters")
    @Pattern(regexp = "[+|\\d][\\d]*", message = "the format of the cellphone is incorrect")
    private String cellphone;

    @NotNull(message = "Date of birth is required")
    private LocalDate dateOfBirth;

    @NotBlank(message = "Email is required")
    @Email(message = "The email format is invalid")
    private String email;

    @NotBlank(message = "Password is required")
    private String password;

    private Long roleId;
}
