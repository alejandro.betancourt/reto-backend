package com.pragma.plazoleta.plazoleta_hexagonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlazoletaHexagonalApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlazoletaHexagonalApplication.class, args);
	}

}
