package com.pragma.plazoleta.plazoleta_hexagonal.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UserResponseDto {

    private Long userId;
    private String name;
    private String lastName;
    private String identificationDocument;
    private String cellphone;
    private LocalDate dateOfBirth;
    private String email;
    private String password;
    private RoleDto role;

}
