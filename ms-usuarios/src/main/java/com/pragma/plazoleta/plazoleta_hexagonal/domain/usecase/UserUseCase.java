package com.pragma.plazoleta.plazoleta_hexagonal.domain.usecase;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IUserServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception.UserNotAdultException;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IUserPersistencePort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;
import java.util.List;

public class UserUseCase implements IUserServicePort {

    private  final IUserPersistencePort userPersistencePort;

    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;

    }

    @Override
    public UserModel saveUser(UserModel userModel) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(isUserAdult(userModel.getDateOfBirth())){
            throw new UserNotAdultException("The user must be of legal age");
        }

        String encodedPassword = bCryptPasswordEncoder.encode(userModel.getPassword()); // encrypt password
        userModel.setPassword(encodedPassword);

        return userPersistencePort.saveUser(userModel);

    }

    @Override
    public UserModel getUserById(Long userId) {
        return userPersistencePort.getUserById(userId);
    }


    @Override
    public List<UserModel> getAllUser() {
        return userPersistencePort.getAllUser();
    }

    private boolean isUserAdult(LocalDate dateOfBirth) {
        return dateOfBirth.isAfter(LocalDate.now().minusYears(18));
    }

}
