package com.pragma.plazoleta.plazoleta_hexagonal.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDto {
    private String name;
}
