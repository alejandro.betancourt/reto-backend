package com.pragma.plazoleta.plazoleta_hexagonal.domain.api;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;

public interface IRoleServicePort {

    RoleModel getRole(Long roleId);
}
