package com.pragma.plazoleta.plazoleta_hexagonal.domain.usecase;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IRoleServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IRolePersistencePort;

public class RoleUseCase implements IRoleServicePort {

    private  final IRolePersistencePort rolePersistencePort;

    public RoleUseCase(IRolePersistencePort rolePersistencePort) {
        this.rolePersistencePort = rolePersistencePort;
    }


    @Override
    public RoleModel getRole(Long roleId) {
        return rolePersistencePort.getRole(roleId);
    }
}
