package com.pragma.plazoleta.plazoleta_hexagonal.application.mapper;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserRequestDto;
import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserResponseDto;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface UserRequestMapper {


    UserModel toUser(UserRequestDto userRequestDto);

    UserResponseDto toDto(UserModel userModel);

}
