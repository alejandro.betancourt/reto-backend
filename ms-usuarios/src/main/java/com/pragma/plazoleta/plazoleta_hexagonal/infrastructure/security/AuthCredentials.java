package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.security;

import lombok.Data;

@Data
public class AuthCredentials {

    private String email;
    private String password;
}
