package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.security;

import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.RoleEntity;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.UserEntity;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IRoleRepository;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IUserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailServiceImpl implements UserDetailsService {


    private final IUserRepository userRepository;
    private final IRoleRepository roleRepository;

    public UserDetailServiceImpl(IUserRepository userRepository, IRoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository
                .findOneByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException("User with Email: " + email + "not exist"));
        List<String> listRoles = roleRepository.findAll().stream().map(RoleEntity::getName).collect(Collectors.toList());
        List<GrantedAuthority> roleAuthority = listRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        return new UserDetailsImpl(userEntity, roleAuthority);
    }
}
