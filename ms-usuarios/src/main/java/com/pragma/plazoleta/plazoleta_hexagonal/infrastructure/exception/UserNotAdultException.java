package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception;

public class UserNotAdultException extends RuntimeException {
    public UserNotAdultException(String message){
        super(message);
    }
}
