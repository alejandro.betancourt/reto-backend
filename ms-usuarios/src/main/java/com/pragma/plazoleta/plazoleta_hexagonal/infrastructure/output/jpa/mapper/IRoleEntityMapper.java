package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IRoleEntityMapper {

    RoleEntity toEntity(RoleModel roleModel);
    RoleModel toRoleModel(RoleEntity roleEntity);
}
