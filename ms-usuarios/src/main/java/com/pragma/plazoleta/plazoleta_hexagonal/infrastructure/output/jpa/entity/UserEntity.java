package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id

    private Long userId;

    private String name;

    private String lastName;

    private String identificationDocument;

    private String cellphone;

    private LocalDate dateOfBirth;

    private String email;

    private String password;

    @ManyToOne
    private RoleEntity role;


}
