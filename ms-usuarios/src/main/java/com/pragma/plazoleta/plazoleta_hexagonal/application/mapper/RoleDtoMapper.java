package com.pragma.plazoleta.plazoleta_hexagonal.application.mapper;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.RoleDto;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RoleDtoMapper {

    RoleDto toRoleDto(RoleModel roleModel);
}

