package com.pragma.plazoleta.plazoleta_hexagonal.domain.api;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;

import java.util.List;

public interface IUserServicePort {

    UserModel saveUser(UserModel userModel);
    UserModel getUserById(Long userId);
    List<UserModel> getAllUser();
}
