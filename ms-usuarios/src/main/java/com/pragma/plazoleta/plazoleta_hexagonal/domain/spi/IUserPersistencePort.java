package com.pragma.plazoleta.plazoleta_hexagonal.domain.spi;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;

import java.util.List;

public interface IUserPersistencePort {
    UserModel saveUser(UserModel userModel);
    UserModel getUserById(Long userId);
    List<UserModel> getAllUser();

}
