package com.pragma.plazoleta.plazoleta_hexagonal.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserModel {

    private Long userId;
    private String name;
    private String lastName;
    private String identificationDocument;
    private String cellphone;
    private LocalDate dateOfBirth;
    private String email;
    private String password;
    private RoleModel role;
}


