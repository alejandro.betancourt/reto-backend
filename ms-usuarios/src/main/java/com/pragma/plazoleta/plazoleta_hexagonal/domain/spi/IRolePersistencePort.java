package com.pragma.plazoleta.plazoleta_hexagonal.domain.spi;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.RoleModel;

public interface IRolePersistencePort {

    RoleModel getRole(Long roleId);
}
