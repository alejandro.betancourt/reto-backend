package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IUserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findOneByEmail(String email);

}
