package com.pragma.plazoleta.plazoleta_hexagonal.application.mapper;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserResponseDto;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        uses = {RoleDtoMapper.class}
)
public interface UserResponseMapper {

    RoleDtoMapper INSTANCE = Mappers.getMapper(RoleDtoMapper.class);

    UserResponseDto toResponse(UserModel user);


    List<UserResponseDto> toResponseList(List<UserModel> userModelList);
}


