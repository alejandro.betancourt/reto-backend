package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.adapter;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IUserPersistencePort;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.UserEntity;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper.IUserEntityMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserJpaAdapter  implements IUserPersistencePort {

    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;


    @Override
    public UserModel saveUser(UserModel userModel) {
        UserEntity userEntity = userRepository.save(userEntityMapper.toEntity(userModel));
        return userEntityMapper.toUserModel(userEntity);
    }

    @Override
    public UserModel getUserById(Long userId) {
        return userEntityMapper.toUserModel(userRepository.getById(userId));
    }

    @Override
    public List<UserModel> getAllUser() {
        List<UserEntity> entityList = userRepository.findAll();
        if (entityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return userEntityMapper.toUserList(entityList);
    }
}
