package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException() {
        super();
    }
}
