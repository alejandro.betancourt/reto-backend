package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.configuration;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IRoleServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.api.IUserServicePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IRolePersistencePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IUserPersistencePort;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.usecase.RoleUseCase;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.usecase.UserUseCase;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.adapter.RoleJpaAdapter;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.adapter.UserJpaAdapter;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper.IRoleEntityMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper.IUserEntityMapper;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IRoleRepository;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;
    private final IRoleRepository roleRepository;
    private  final IRoleEntityMapper roleEntityMapper;


    @Bean
    public IUserPersistencePort userPersistencePort() {
        return new UserJpaAdapter(userRepository, userEntityMapper);
    }

    @Bean
    public IUserServicePort userServicePort() {
        return new UserUseCase(userPersistencePort());
    }
    @Bean
    public IRolePersistencePort rolePersistencePort() {
        return new RoleJpaAdapter(roleRepository, roleEntityMapper);
    }

    @Bean
    public IRoleServicePort roleServicePort() {
        return new RoleUseCase(rolePersistencePort());
    }

}
