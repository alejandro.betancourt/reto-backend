package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class TokenUtils {

    private static final  String ACCESS_TOKEN_SECRET = "4Q8u39fZdWIWde8qZv5P57TG9WAmSo33";
    private static final  Long ACCESS_TOKEN_VALIDITY_SECONDS = 2_592_000L;

    public static String createToken(UserDetailsImpl userDetails){
        long expirationTime = ACCESS_TOKEN_VALIDITY_SECONDS * 1_000;
        Date expirationDate = new Date(System.currentTimeMillis() + expirationTime);

        Map<String,Object> extra = new HashMap<>();
        extra.put("name", userDetails.getName());
        extra.put("role", userDetails.getRole());
        extra.put("id", userDetails.getId());

        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setExpiration(expirationDate)
                .addClaims(extra)
                .signWith(Keys.hmacShaKeyFor(ACCESS_TOKEN_SECRET.getBytes()))
                .compact();
    }

    public static UsernamePasswordAuthenticationToken getAuthentication(String token){
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(ACCESS_TOKEN_SECRET.getBytes())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();

            String email = claims.getSubject();
            String roles = (String) claims.get("role");
            List<GrantedAuthority> authorityList = Collections.singletonList(new SimpleGrantedAuthority(roles));

            return new UsernamePasswordAuthenticationToken(email, null, authorityList);
        } catch (JwtException e){
            return null;
        }
    }

}

