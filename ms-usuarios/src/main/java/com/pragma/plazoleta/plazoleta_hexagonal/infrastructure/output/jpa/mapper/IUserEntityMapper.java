package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.mapper;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IUserEntityMapper {

    UserEntity toEntity(UserModel user);
    UserModel toUserModel(UserEntity userEntity);
    List<UserModel> toUserList(List<UserEntity> userEntityList);
}
