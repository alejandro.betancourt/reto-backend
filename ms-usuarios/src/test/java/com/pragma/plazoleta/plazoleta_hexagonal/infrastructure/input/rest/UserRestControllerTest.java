package com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.input.rest;

import com.pragma.plazoleta.plazoleta_hexagonal.application.dto.UserRequestDto;
import com.pragma.plazoleta.plazoleta_hexagonal.application.handler.IUserHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserRestControllerTest {

    @Mock
    private IUserHandler userHandler;

    @InjectMocks
    private UserRestController userRestController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveEmployee_Success() {

        UserRequestDto userRequestDto = new UserRequestDto();
        userRequestDto.setName("John");
        userRequestDto.setLastName("Lopez");
        userRequestDto.setIdentificationDocument("1002959700");
        userRequestDto.setCellphone("+573158502904");
        userRequestDto.setDateOfBirth(LocalDate.of(2000,4,24));
        userRequestDto.setEmail("john@gmail.com");
        userRequestDto.setPassword("john");
        userRequestDto.setRoleId(4L);

        doNothing().when(userHandler).saveUser(userRequestDto);

        ResponseEntity<Void> response = userRestController.saveEmployee(userRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());

        verify(userHandler, times(1)).saveUser(userRequestDto);

    }

    @Test
    void testSaveCustomer() {
        // Arrange
        UserRequestDto userRequestDto = new UserRequestDto();
        userRequestDto.setName("Jhoan");
        userRequestDto.setLastName("Perez");
        userRequestDto.setIdentificationDocument("1002959700");
        userRequestDto.setCellphone("+573158502904");
        userRequestDto.setDateOfBirth(LocalDate.of(2000,4,24));
        userRequestDto.setEmail("jhoan@gmail.com");
        userRequestDto.setPassword("jhoan");
        userRequestDto.setRoleId(3L);

        doNothing().when(userHandler).saveUser(userRequestDto);

        ResponseEntity<Void> response = userRestController.saveCustomer(userRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(userHandler, times(1)).saveUser(userRequestDto);

        verifyNoMoreInteractions(userHandler);
    }



}