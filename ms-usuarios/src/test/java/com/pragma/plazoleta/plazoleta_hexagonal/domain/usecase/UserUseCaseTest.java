package com.pragma.plazoleta.plazoleta_hexagonal.domain.usecase;

import com.pragma.plazoleta.plazoleta_hexagonal.domain.model.UserModel;
import com.pragma.plazoleta.plazoleta_hexagonal.domain.spi.IUserPersistencePort;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.exception.UserNotAdultException;
import com.pragma.plazoleta.plazoleta_hexagonal.infrastructure.output.jpa.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserUseCaseTest {

    @Mock
    private IUserPersistencePort userPersistencePort;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private UserUseCase userUseCase;
    private UserModel userModel;
    private UserEntity userEntity;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this); // start the mocks
    }

    @DisplayName("Test to save user correctly")
    @Test
    void testSaveUser() {
        UserModel userModel = new UserModel();
        userModel.setDateOfBirth(LocalDate.of(2000, 1, 1));
        userModel.setPassword("password123");

        when(userPersistencePort.saveUser(any(UserModel.class))).thenReturn(userModel);

        userUseCase.saveUser(userModel);

        verify(userPersistencePort, times(1)).saveUser(any(UserModel.class));
    }


    @DisplayName("Test to compare password encrypt")
    @Test
     void testSaveEncryptPassword(){
        userModel = new UserModel();
        userEntity = new UserEntity();
        userModel.setDateOfBirth(LocalDate.of(2015,4,28));
        userModel.setPassword("myPassword");

        when(userPersistencePort.saveUser(any(UserModel.class))).thenReturn(userModel);
        assertNotNull(userModel.getPassword());
        assertNotEquals("myPassword", userEntity.getPassword());

    }



    @DisplayName("Test to try save user with illegal age")
    @Test
    void testSaveUserWithIllegalAge(){
        userModel = new UserModel();
        userModel.setDateOfBirth(LocalDate.of(2015,4,28));
        userModel.setPassword("password1231");

        when(userPersistencePort.saveUser(any(UserModel.class))).thenReturn(userModel);
        assertNotNull(userModel.getDateOfBirth());
        assertThrows(UserNotAdultException.class, () -> userUseCase.saveUser(userModel));
    }
}