package com.pragma.powerup.messaging.infrastructure.output.jpa.adapter;

import com.pragma.powerup.messaging.domain.models.TwilioModel;
import com.pragma.powerup.messaging.domain.spi.TwilioPersistencePort;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.stereotype.Component;

@Component
public class TwilioMessagingAdapter implements TwilioPersistencePort {

    private static final  String ACCOUNT_SID = "AC020ac7a452370e8abf453c20681ad809";
    private static final String AUTH_TOKEN = "aba9f99472f849945fbe6472e9a4d116";
    private static final String TWILIO_PHONE_NUMBER = "+19843052561";

    @Override
    public void sendMessage(TwilioModel twilioModel) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message.creator(
                new PhoneNumber(twilioModel.getPhoneNumber()),
                new PhoneNumber(TWILIO_PHONE_NUMBER),
                twilioModel.getMessage()
        ).create();
    }
}
