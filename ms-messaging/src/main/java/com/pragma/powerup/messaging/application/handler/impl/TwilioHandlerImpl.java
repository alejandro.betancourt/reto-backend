package com.pragma.powerup.messaging.application.handler.impl;

import com.pragma.powerup.messaging.application.dto.TwilioRequestDto;
import com.pragma.powerup.messaging.application.handler.TwilioHandler;
import com.pragma.powerup.messaging.application.mapper.TwilioRequestMapper;
import com.pragma.powerup.messaging.domain.api.TwilioServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TwilioHandlerImpl implements TwilioHandler {

    private final TwilioServicePort twilioServicePort;
    private final TwilioRequestMapper twilioRequestMapper;

    @Override
    public void sendMessage(TwilioRequestDto twilioRequest) {
        twilioServicePort.sendMessage(
                twilioRequestMapper.toModel(twilioRequest)
        );
    }
}
