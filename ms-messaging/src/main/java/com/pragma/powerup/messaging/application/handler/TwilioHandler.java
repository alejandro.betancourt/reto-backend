package com.pragma.powerup.messaging.application.handler;

import com.pragma.powerup.messaging.application.dto.TwilioRequestDto;

public interface TwilioHandler {

    void sendMessage(TwilioRequestDto twilioRequest);

}
