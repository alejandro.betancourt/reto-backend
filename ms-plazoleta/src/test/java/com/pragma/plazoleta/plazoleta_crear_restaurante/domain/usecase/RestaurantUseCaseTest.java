package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RoleDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.UserDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.InvalidUserRoleException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoUserFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.UserFeignClient;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import feign.FeignException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RestaurantUseCaseTest {

    @Mock
    private IRestaurantPersistencePort restaurantPersistencePort;

    @Mock
    private IRestaurantEntityMapper restaurantEntityMapper;

    @Mock
    private UserFeignClient userFeignClient;

    @InjectMocks
    private RestaurantUseCase restaurantUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveRestaurant_InvalidUserRole() {

        UserDto userDto = new UserDto();
        RoleDto roleDto = new RoleDto();
        roleDto.setName("cliente");
        userDto.setRole(roleDto);

        when(userFeignClient.getUserById(3L)).thenReturn(ResponseEntity.ok(userDto));

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setUserId(3L);

        assertThrows(InvalidUserRoleException.class, () -> {
            restaurantUseCase.saveRestaurant(restaurantModel);
        });
    }


    @Test
    void testSaveRestaurant_NoUserFound() {
        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setName("My Restaurant");
        restaurantModel.setNit("123456789");
        restaurantModel.setAddress("123 Main St");
        restaurantModel.setCellphone("1234567890");
        restaurantModel.setUrlLogo("http://example.com/logo.png");
        restaurantModel.setUserId(1L);

        when(userFeignClient.getUserById(anyLong())).thenThrow(FeignException.class);


        assertThrows(NoUserFoundException.class, () -> {
            restaurantUseCase.saveRestaurant(restaurantModel);
        });
    }

    @Test
    void testSaveRestaurant_ValidUserRole() {

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setUserId(2L);

        UserDto userDto = new UserDto();
        RoleDto roleDto = new RoleDto();
        roleDto.setName("propietario");
        userDto.setRole(roleDto);

        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));

        RestaurantEntity restaurantEntity = new RestaurantEntity();
        when(restaurantEntityMapper.toEntity(any(RestaurantModel.class))).thenReturn(restaurantEntity);

        RestaurantModel savedRestaurantModel = new RestaurantModel();
        when(restaurantEntityMapper.toRestaurantModel(any(RestaurantEntity.class))).thenReturn(savedRestaurantModel);

        when(restaurantPersistencePort.saveRestaurant(any(RestaurantModel.class))).thenReturn(savedRestaurantModel);

        RestaurantModel result = restaurantUseCase.saveRestaurant(restaurantModel);

        assertNotNull(result);
        assertEquals(savedRestaurantModel, result);
    }

    @Test
    void testSaveRestaurant_NoUserFoundException() {
        RestaurantModel restaurantModel = new RestaurantModel();

        when(userFeignClient.getUserById(anyLong())).thenReturn(null);

        assertThrows(NoUserFoundException.class, () -> {
            restaurantUseCase.saveRestaurant(restaurantModel);
        });
    }

    @Test
    void testListAndPaginateRestaurants_AlphabeticalOrder() {
        List<RestaurantModel> mockRestaurantList = Arrays.asList(
                new RestaurantModel(1L, "Abasos", "54455545", "Cr4 15-873", "+573158502904", "dedfefef/54554/hgtgtg", 3L),
                new RestaurantModel(2L, "Betafc", "57855545", "Cr7 15-873", "+573158504674", "dfdeddqg/54554/hgtgtg", 7L)
        );

        when(restaurantPersistencePort.getPaginatedAndSortedRestaurants(anyInt(), anyInt()))
                .thenReturn(mockRestaurantList);

        int page = 0;
        int size = 10;
        List<RestaurantModel> resultPage = restaurantUseCase.getPaginatedAndSortedRestaurants(page, size);

        verify(restaurantPersistencePort).getPaginatedAndSortedRestaurants(page,size);
        assertEquals(mockRestaurantList.size(), resultPage.size());
        assertEquals(mockRestaurantList.get(0).getName(), resultPage.get(0).getName());
        assertEquals(mockRestaurantList.get(1).getName(), resultPage.get(1).getName());
    }

    @Test
    void testListAndPaginateRestaurants_ReturnedFields() {

        List<RestaurantModel> mockRestaurantList = Arrays.asList(
                new RestaurantModel(1L, "Abasos", "54455545", "Cr4 15-873", "+573158502904", "dedfefef/54554/hgtgtg", 3L),
                new RestaurantModel(2L, "Betafc", "57855545", "Cr7 15-873", "+573158504674", "dfdeddqg/54554/hgtgtg", 7L)
        );

        when(restaurantPersistencePort.getPaginatedAndSortedRestaurants(anyInt(), anyInt()))
                .thenReturn(mockRestaurantList);

        int page = 0;
        int size = 5;
        List<RestaurantModel> resultPage = restaurantUseCase.getPaginatedAndSortedRestaurants(page, size);

        verify(restaurantPersistencePort).getPaginatedAndSortedRestaurants(page, size);


        for (RestaurantModel restaurantModel : resultPage) {
            assertNotNull(restaurantModel.getName());
            assertNotNull(restaurantModel.getUrlLogo());
        }
    }

}