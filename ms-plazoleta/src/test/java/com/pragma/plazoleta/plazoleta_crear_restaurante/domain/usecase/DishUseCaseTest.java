package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishUpdateDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.DishUpdateMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IDishPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDishFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoRestaurantFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IDishEntityMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DishUseCaseTest {

    @Mock
    private IDishPersistencePort dishPersistencePort;

    @Mock
    private IRestaurantPersistencePort restaurantPersistencePort;

    @InjectMocks
    private DishUseCase dishUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSaveDish_NoRestaurantFound() {
        DishModel dishModel = new DishModel();
        dishModel.setRestaurantId(222L);

        when(restaurantPersistencePort.getRestaurant(dishModel.getRestaurantId())).thenReturn(null);

        assertThrows(NoRestaurantFoundException.class, () -> {
            dishUseCase.saveDish(dishModel);
        });
    }

    @Test
    void testSaveDish_RestaurantFound() {
        DishModel dishModel = new DishModel();
        dishModel.setRestaurantId(10L);

        RestaurantModel mockRestaurant = new RestaurantModel();
        mockRestaurant.setUserId(2L);

        when(restaurantPersistencePort.getRestaurant(dishModel.getRestaurantId())).thenReturn(mockRestaurant);


        when(dishPersistencePort.getIdByAuthentication()).thenReturn(2L);

        when(dishPersistencePort.saveDish(any(DishModel.class))).thenReturn(dishModel);

        DishModel savedDish = dishUseCase.saveDish(dishModel);

        assertNotNull(savedDish);

        verify(restaurantPersistencePort).getRestaurant(10L);
        verify(dishPersistencePort).saveDish(dishModel);
        verify(dishPersistencePort).getIdByAuthentication();
    }

    @Test
    void testGetAllDish() {
        
        List<DishModel> dishModels = new ArrayList<>();
        dishModels.add(new DishModel());
        dishModels.add(new DishModel());
        
        when(dishPersistencePort.getAllDish()).thenReturn(dishModels);
        
        List<DishModel> result = dishUseCase.getAllDish();

        verify(dishPersistencePort, times(1)).getAllDish();

        assertEquals(dishModels, result);
    }

    @Test
    void testUpdateDish_DishNotFound() {
        Long dishId = 2L;
        DishModel dishModel = new DishModel();
        dishModel.setDishId(dishId);

        when(dishPersistencePort.getDish(dishId)).thenReturn(null);

        assertThrows(NoDishFoundException.class, () -> {
            dishUseCase.updateDish(dishModel);
        });
    }

    @Test
    void testUpdateDish_UpdatePrice() {
        Long restaurantId = 3L;
        Long dishId = 8L;
        Integer newPrice = 15000;

        DishModel mockModel = new DishModel();
        mockModel.setDishId(dishId);
        mockModel.setPrice(2100);
        mockModel.setName("Chuleta");
        mockModel.setState(true);
        mockModel.setCategory("Proteina");
        mockModel.setUrlImage("ddf/23243/sss");
        mockModel.setRestaurantId(restaurantId);

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setRestaurantId(restaurantId);
        restaurantModel.setUserId(5L);

        when(dishPersistencePort.getDish(dishId)).thenReturn(mockModel);
        when(restaurantPersistencePort.getRestaurant(restaurantId)).thenReturn(restaurantModel);
        when(dishPersistencePort.getIdByAuthentication()).thenReturn(5L);
        mockModel.setDishId(dishId);
        mockModel.setPrice(newPrice);
        when(dishPersistencePort.updateDish(mockModel)).thenReturn(mockModel);

        DishModel result = dishUseCase.updateDish(mockModel);

        assertEquals(newPrice, result.getPrice());
        assertNull(result.getDescription());
    }

    @Test
    void testUpdateDish_SuccessfulUpdate() {
        Long restaurantId = 3L;
        Long dishId = 8L;
        Integer newPrice = 15000;

        DishModel mockModel = new DishModel();
        mockModel.setDishId(dishId);
        mockModel.setPrice(5500);
        mockModel.setName("Carne y guacamole");
        mockModel.setState(true);
        mockModel.setCategory("Proteina");
        mockModel.setUrlImage("ddf/23243/sss");
        mockModel.setRestaurantId(restaurantId);

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setRestaurantId(restaurantId);
        restaurantModel.setUserId(7L);

        when(dishPersistencePort.getDish(dishId)).thenReturn(mockModel);
        when(restaurantPersistencePort.getRestaurant(restaurantId)).thenReturn(restaurantModel);
        when(dishPersistencePort.getIdByAuthentication()).thenReturn(7L);
        mockModel.setDishId(dishId);
        mockModel.setPrice(newPrice);
        when(dishPersistencePort.updateDish(mockModel)).thenReturn(mockModel);

        DishModel result = dishUseCase.updateDish(mockModel);

        assertEquals(newPrice, result.getPrice());
        assertNull(result.getDescription());
    }

    @Test
    void testGetPaginatedAndSortedDishes() {
        int page = 1;
        int size = 10;
        String category = "Category";

        List<DishModel> dummyDishes = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            dummyDishes.add(new DishModel());
        }

        when(dishPersistencePort.getPaginatedAndSortedDishes(page, size, category))
                .thenReturn(dummyDishes);

        List<DishModel> result = dishUseCase.getPaginatedAndSortedDishes(page, size, category);

        assertEquals(size, result.size(), "The size of the resulting list is not as expected");

        verify(dishPersistencePort, times(1))
                .getPaginatedAndSortedDishes(page, size, category);
    }


    @Test
    void testGetPaginatedAndSortedDishesNoDataFound() {
        int page = 1;
        int size = 10;
        String category = "Category";

        when(dishPersistencePort.getPaginatedAndSortedDishes(page, size, category))
                .thenReturn(Collections.emptyList());

        assertThrows(NoDataFoundException.class, () ->
                dishUseCase.getPaginatedAndSortedDishes(page, size, category)
        );

        verify(dishPersistencePort, times(1))
                .getPaginatedAndSortedDishes(page, size, category);
    }

}