package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.UserDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.fpi.TwilioFeignClientPort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.TwilioModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IOrderPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoCreateOrderException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoOrderFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.UserFeignClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderUseCaseTest {

    @Mock
    private IOrderPersistencePort orderPersistencePort;

    @Mock
    private UserFeignClient userFeignClient;

    @Mock
    private TwilioFeignClientPort twilioFeignClientPort;

    @InjectMocks
    private OrderUseCase orderUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
     void testSaveOrder_SuccessfulOrderCreation() {

        OrderDishModel orderDishModel = new OrderDishModel();
        OrderModel orderModel = new OrderModel();
        orderModel.setRestaurantId(5L);
        orderDishModel.setOrderModel(orderModel);

        when(orderPersistencePort.getIdByAuthentication()).thenReturn(1L);
        when(orderPersistencePort.getOrderByUserId(1L)).thenReturn(Collections.emptyList());


        orderUseCase.saveOrder(orderDishModel);

        verify(orderPersistencePort).getIdByAuthentication();
        verify(orderPersistencePort).getOrderByUserId(1L);

        verify(orderPersistencePort).saveOrder(ArgumentMatchers.any(OrderModel.class));
        verify(orderPersistencePort).saveOrderDetails(orderDishModel);
    }

    @Test
    void testSaveOrder_OrderInProgressException() {
        OrderDishModel orderDishModel = new OrderDishModel();
        OrderModel orderModel = new OrderModel();
        orderModel.setRestaurantId(5L);
        orderDishModel.setOrderModel(orderModel);

        when(orderPersistencePort.getIdByAuthentication()).thenReturn(1L);

        OrderModel orderInProgress = new OrderModel();
        orderInProgress.setState("En_proceso");

        when(orderPersistencePort.getOrderByUserId(1L)).thenReturn(Collections.singletonList(orderInProgress));

        assertThrows(NoCreateOrderException.class, () -> orderUseCase.saveOrder(orderDishModel));

        verify(orderPersistencePort).getIdByAuthentication();
        verify(orderPersistencePort).getOrderByUserId(1L);
        verifyNoMoreInteractions(orderPersistencePort);
    }

    @Test
    void testSaveOrder_EmptyOrderList() {
        OrderDishModel orderDishModel = new OrderDishModel();
        OrderModel orderModel = new OrderModel();
        orderModel.setRestaurantId(5L);
        orderDishModel.setOrderModel(orderModel);

        when(orderPersistencePort.getIdByAuthentication()).thenReturn(1L);

        when(orderPersistencePort.getOrderByUserId(1L)).thenReturn(Collections.emptyList());

        orderUseCase.saveOrder(orderDishModel);

        verify(orderPersistencePort).getIdByAuthentication();
        verify(orderPersistencePort).getOrderByUserId(1L);
        verify(orderPersistencePort).saveOrder(any(OrderModel.class));
        verify(orderPersistencePort).saveOrderDetails(orderDishModel);
        verifyNoMoreInteractions(orderPersistencePort);
    }

    @Test
    void testGetPaginatedAndSortedRestaurants() {
        int page = 0;
        int size = 10;
        String state = "Pendiente";

        OrderModel order1 = new OrderModel();
        order1.setOrderId(29L);
        order1.setUserId(12L);
        order1.setDate(LocalDate.parse("2023-08-30"));
        order1.setState("Pendiente");
        order1.setChefId(null);
        order1.setRestaurantId(null);

        List<OrderModel> mockOrderList = new ArrayList<>();
        mockOrderList.add(order1);

        when(orderPersistencePort.getPaginatedAndSortedRestaurants(page, size, state))
                .thenReturn(mockOrderList);

        List<OrderModel> result = orderUseCase.getPaginatedAndSortedRestaurants(page, size, state);

        assertEquals(mockOrderList, result);
        verify(orderPersistencePort, times(1)).getPaginatedAndSortedRestaurants(page, size, state);
    }

    @Test
     void testGetPaginatedAndSortedRestaurantsNoDataFoundException() {

        OrderUseCase orderUseCase = new OrderUseCase(orderPersistencePort,twilioFeignClientPort, userFeignClient);

        int page = 0;
        int size = 10;
        String state = "Pendiente";
        when(orderPersistencePort.getPaginatedAndSortedRestaurants(page, size, state))
                .thenReturn(new ArrayList<>());

        assertThrows(NoDataFoundException.class,
                () -> orderUseCase.getPaginatedAndSortedRestaurants(page, size, state));
    }

    @Test
    void testUpdateStateOrder_Success() {
        Long orderId = 2L;
        String newState = "En preparacion";
        Long chefId = 5L;

        OrderModel existingOrder = new OrderModel();
        existingOrder.setOrderId(orderId);
        existingOrder.setState("Pendiente");

        OrderModel updatedOrder = new OrderModel();
        updatedOrder.setOrderId(orderId);
        updatedOrder.setState(newState);
        updatedOrder.setChefId(chefId);

        when(orderPersistencePort.getOrder(orderId)).thenReturn(existingOrder);
        when(orderPersistencePort.updateStateOrder(orderId, existingOrder)).thenReturn(updatedOrder);

        OrderModel result = orderUseCase.updateStateOrder(orderId, updatedOrder);

        assertEquals(newState, result.getState());
        assertEquals(chefId, result.getChefId());

        verify(orderPersistencePort, times(1)).getOrder(orderId);
        verify(orderPersistencePort, times(1)).updateStateOrder(orderId, existingOrder);
    }

    @Test
    void testUpdateOrderPreparation_EnPreparacion() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("En_preparacion");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");

        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));
        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        orderUseCase.updateOrderPreparation(orderId);

        assertEquals("Listo", orderModelDb.getState());
        verify(twilioFeignClientPort, never()).sendMessage(any(TwilioModel.class));
        verify(orderPersistencePort, times(1)).updateStateOrder(orderId, orderModelDb);
    }

    @Test
    @DisplayName("Get Phone Number by Feign")
    void testGetPhoneNumberByFeign() {
        Long userId = 4L;
        UserDto userDto = new UserDto();
        userDto.setUserId(userId);
        userDto.setCellphone("+573158502904");

        when(userFeignClient.getUserById(userId)).thenReturn(ResponseEntity.ok(userDto));

        String phoneNumber = orderUseCase.getPhoneNumberByFeign(userId);

        assertEquals("+573158502904", phoneNumber);
    }

    @Test
    void testUpdateOrderPreparation_Listo() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("Listo");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");

        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));
        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        orderUseCase.updateOrderPreparation(orderId);

        assertEquals("Entregado", orderModelDb.getState());
        verify(twilioFeignClientPort, times(1)).sendMessage(any(TwilioModel.class));
        verify(orderPersistencePort, times(1)).updateStateOrder(orderId, orderModelDb);
    }

    @Test
    void testUpdateOrderPreparation_Entregado() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("Entregado");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");
        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));

        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        try {
            orderUseCase.updateOrderPreparation(orderId);
        } catch (NoOrderFoundException ex) {
            assertEquals("A delivered order cannot be modified.", ex.getMessage());
        }

        verify(twilioFeignClientPort, never()).sendMessage(any(TwilioModel.class));
        verify(orderPersistencePort, never()).updateStateOrder(orderId, orderModelDb);
    }

    @Test
    void testUpdateOrderPreparation_NotReadyToBeDelivered() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("Cancelado");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");
        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));

        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        assertThrows(NoOrderFoundException.class, () -> {
            orderUseCase.updateOrderPreparation(orderId);
        });

        verify(twilioFeignClientPort, never()).sendMessage(any());
        verify(orderPersistencePort, never()).updateStateOrder(anyLong(), any(OrderModel.class));
    }


    @Test
    void testUpdateOrderPreparation_WhenOrderIsNull_ShouldThrowNoOrderFoundException() {

        Long orderId = 15L;

        when(orderPersistencePort.getOrder(orderId)).thenReturn(null);

        try {
            orderUseCase.updateOrderPreparation(orderId);
            fail("Expected NoOrderFoundException to be thrown");
        } catch (NoOrderFoundException ex) {
            assertEquals("Order not found", ex.getMessage());
        }
    }

    @Test
    void testCancelOrder_Pendiente() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("Pendiente");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");
        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));

        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        orderUseCase.cancelOrder(orderId);

        assertEquals("Cancelado", orderModelDb.getState());


        verify(twilioFeignClientPort, times(1)).sendMessage(any(TwilioModel.class));
        verify(orderPersistencePort, times(1)).updateStateOrder(orderId, orderModelDb);
    }

    @Test
    void testCancelOrder_OrderNotFound() {
        Long orderId = 1L;
        when(orderPersistencePort.getOrder(orderId)).thenReturn(null);

        assertThrows(NoOrderFoundException.class, () -> {
            orderUseCase.cancelOrder(orderId);
        });
    }

    @Test
    void testCancelOrder_NotPendiente() {
        Long orderId = 1L;
        OrderModel orderModelDb = new OrderModel();
        orderModelDb.setUserId(2L);
        orderModelDb.setState("En_preparacion");

        UserDto userDto = new UserDto();
        userDto.setUserId(2L);
        userDto.setCellphone("+573158502904");
        when(userFeignClient.getUserById(2L)).thenReturn(ResponseEntity.ok(userDto));

        when(orderPersistencePort.getOrder(orderId)).thenReturn(orderModelDb);

        try {
            orderUseCase.cancelOrder(orderId);
            fail("Expected NoOrderFoundException");
        } catch (NoOrderFoundException ex) {
            assertEquals("Sorry, your order is already being prepared and cannot be canceled", ex.getMessage());
        }

        verify(twilioFeignClientPort, never()).sendMessage(any(TwilioModel.class));
        verify(orderPersistencePort, never()).updateStateOrder(orderId, orderModelDb);
    }

}