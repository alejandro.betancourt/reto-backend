package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class TwilioFeignClientException extends RuntimeException{

    public TwilioFeignClientException(String message) {
        super(message);
    }
}
