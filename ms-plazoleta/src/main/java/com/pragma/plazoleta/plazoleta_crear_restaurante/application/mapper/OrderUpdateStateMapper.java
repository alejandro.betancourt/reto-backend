package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderUpdateStateDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface OrderUpdateStateMapper {

    OrderModel toOrder(OrderUpdateStateDto orderUpdateStateDto);

}
