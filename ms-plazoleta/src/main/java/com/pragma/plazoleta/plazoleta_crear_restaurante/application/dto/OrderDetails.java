package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails {

    private Long dishId;
    private Integer amount;
}
