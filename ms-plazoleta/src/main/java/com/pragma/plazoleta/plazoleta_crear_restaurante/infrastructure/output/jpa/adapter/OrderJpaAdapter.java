package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IOrderPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.utils.ExtractAuthenticationData;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IOrderEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IOrderEntityPaginatedMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.IOrderDishRepository;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.IOrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;
import java.util.List;

@RequiredArgsConstructor
@Transactional
public class OrderJpaAdapter implements IOrderPersistencePort {

    private final IOrderRepository orderRepository;
    private final IOrderDishRepository orderDishRepository;
    private final IOrderEntityMapper orderEntityMapper;
    private final IOrderEntityPaginatedMapper orderEntityPaginatedMapper;

    @Override
    public OrderModel saveOrder(OrderModel orderModel) {
        return orderEntityMapper.toOrderModel(orderRepository.save(orderEntityMapper.toOrderEntity(orderModel)));
    }

    @Override
    public void saveOrderDetails(OrderDishModel orderDishModel) {
        orderDishRepository.saveAll(orderEntityMapper.toOrderDishEntity(orderDishModel));
    }

    @Override
    public OrderModel getOrder(Long orderId) {
        return orderEntityMapper.toOrderModel(orderRepository.findById(orderId).orElse(null));
    }

    @Override
    public Long getIdByAuthentication() {
        return ExtractAuthenticationData.getIdByAuthentication();
    }

    @Override
    public List<OrderModel> getOrderByUserId(Long userId) {
        return orderEntityMapper.toOrderModelList(orderRepository.findOrderEntityByUserId(userId));
    }

    @Override
    public List<OrderModel> getAllOrder() {
        return null;
    }

    @Override
    public List<OrderModel> getPaginatedAndSortedRestaurants(int page, int size, String state) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.by("state").ascending());
        Page<OrderEntity> orderEntityPage = orderRepository.findAllByStateContainingIgnoreCase(pageRequest, state);
        return orderEntityPaginatedMapper.toOrderList(orderEntityPage.getContent());
    }

    @Override
    public OrderModel updateStateOrder(Long orderId, OrderModel orderModel) {
        OrderEntity orderEntity = orderRepository.save(orderEntityMapper.toOrderEntity(orderModel));
        return orderEntityMapper.toOrderModel(orderEntity);
    }


}
