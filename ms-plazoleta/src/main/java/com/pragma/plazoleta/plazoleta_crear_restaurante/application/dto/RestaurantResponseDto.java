package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantResponseDto {

    private Long restaurantId;
    private String name;
    private String nit;
    private String address;
    private String cellphone;
    private String urlLogo;
    private Long userId;
}
