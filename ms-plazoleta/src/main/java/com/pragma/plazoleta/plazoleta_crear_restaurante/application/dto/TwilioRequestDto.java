package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TwilioRequestDto {

    private String message;
    private String phoneNumber;
}
