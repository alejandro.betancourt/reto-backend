package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoCreateOrderException extends RuntimeException{

    public NoCreateOrderException(String message){
        super(message);
    }
}
