package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishModel {

    private Long dishId;
    private String name;
    private Integer price;
    private String description;
    private String urlImage;
    private String category;
    private Boolean state;
    private Long restaurantId;
}
