package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class RestaurantNoBelongOwnerException extends RuntimeException{

    public RestaurantNoBelongOwnerException(String message){
        super(message);
    }
}
