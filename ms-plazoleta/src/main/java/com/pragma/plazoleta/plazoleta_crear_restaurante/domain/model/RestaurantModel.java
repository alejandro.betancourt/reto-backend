package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantModel {

    private Long restaurantId;
    private String name;
    private String nit;
    private String address;
    private String cellphone;
    private String urlLogo;
    private Long userId;

}
