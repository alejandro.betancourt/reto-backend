package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;

import java.util.List;

public interface IRestaurantServicePort {

    RestaurantModel saveRestaurant(RestaurantModel restaurantModel);

    RestaurantModel getRestaurant(Long restaurantId);

    List<RestaurantModel> getAllRestaurant();

    List<RestaurantModel> getPaginatedAndSortedRestaurants(int page, int size);

}
