package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
public interface IRestaurantPaginatedRepository extends PagingAndSortingRepository<RestaurantEntity, Long>{
}
