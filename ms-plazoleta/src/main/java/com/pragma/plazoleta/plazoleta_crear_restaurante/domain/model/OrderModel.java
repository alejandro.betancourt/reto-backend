package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderModel {

    private Long orderId;
    private Long userId;
    private LocalDate date;
    private String state;
    private Long chefId;
    private Long restaurantId;

}
