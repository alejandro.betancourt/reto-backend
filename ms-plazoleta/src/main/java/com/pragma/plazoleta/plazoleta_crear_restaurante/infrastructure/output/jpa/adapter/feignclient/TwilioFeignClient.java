package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.TwilioRequestDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "messaging-microservice", url = "http://localhost:8092")
public interface TwilioFeignClient {

    @PostMapping("/twilio/message")
    ResponseEntity<Map<String, String>> sendMessage(@RequestBody TwilioRequestDto twilioRequest);

}
