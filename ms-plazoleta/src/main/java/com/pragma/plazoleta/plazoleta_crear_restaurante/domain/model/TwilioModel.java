package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TwilioModel {

    private String message;
    private String phoneNumber;
}
