package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IDishServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IDishPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDishFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoRestaurantFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.RestaurantNoBelongOwnerException;

import java.util.List;
import java.util.Objects;

public class DishUseCase implements IDishServicePort {

    private final IDishPersistencePort dishPersistencePort;
    private final IRestaurantPersistencePort restaurantPersistencePort;

    public DishUseCase(IDishPersistencePort dishPersistencePort, IRestaurantPersistencePort restaurantPersistencePort) {
        this.dishPersistencePort = dishPersistencePort;
        this.restaurantPersistencePort = restaurantPersistencePort;
    }

    @Override
    public DishModel saveDish(DishModel dishModel) {
        restaurantAndOwnerValidation(dishModel);
        dishModel.setState(true);
        return dishPersistencePort.saveDish(dishModel);
    }

    @Override
    public DishModel updateDish(DishModel dishModel) {
        DishModel dishModelDb = getDish(dishModel.getDishId());
        restaurantAndOwnerValidation(dishModelDb);
        dishModelDb.setPrice(dishModel.getPrice());
        dishModelDb.setDescription(dishModel.getDescription());

        return dishPersistencePort.updateDish(dishModelDb);
    }

    @Override
    public DishModel updateStateDish(Long dishId, DishModel dishModel) {
        DishModel disModelDb = dishPersistencePort.getDish(dishId);
        restaurantAndOwnerValidation(disModelDb);
        disModelDb.setState(dishModel.getState());

        return dishPersistencePort.updateStateDish(dishId, disModelDb);
    }

    @Override
    public DishModel getDish(Long dishId) {
        DishModel dishModel = dishPersistencePort.getDish(dishId);
        if(dishModel == null){
            throw new NoDishFoundException("Dish not found");
        }
        return dishPersistencePort.getDish(dishId);
    }

    @Override
    public List<DishModel> getAllDish() {
        List<DishModel> list =  dishPersistencePort.getAllDish();
        if (list.isEmpty()) {
            throw new NoDataFoundException("Not dish found");
        }
        return list;
    }

    @Override
    public List<DishModel> getPaginatedAndSortedDishes(int page, int size, String category) {
        List<DishModel> list =  dishPersistencePort.getPaginatedAndSortedDishes(page, size, category);
        if (list.isEmpty()) {
            throw new NoDataFoundException("Not dish found");
        }
        return list;
    }

    private void restaurantAndOwnerValidation(DishModel dishModel){

        Long restaurantId = dishModel.getRestaurantId();

        RestaurantModel restaurantModel = restaurantPersistencePort.getRestaurant(restaurantId);
        if(restaurantModel == null){
            throw new NoRestaurantFoundException("Restaurant not found");
        }
        if (!Objects.equals(restaurantModel.getUserId(), dishPersistencePort.getIdByAuthentication())){
            throw new RestaurantNoBelongOwnerException("Restaurant does not belong to owner");
        }
    }

}
