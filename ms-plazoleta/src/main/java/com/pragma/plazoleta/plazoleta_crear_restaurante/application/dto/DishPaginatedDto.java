package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishPaginatedDto {

    private String name;
    private Integer price;
    private String description;
    private String urlImage;
    private String category;
    private Boolean state;
}
