package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRestaurantRepository extends JpaRepository<RestaurantEntity, Long> {

    boolean existsByUserId(Long userId);

}
