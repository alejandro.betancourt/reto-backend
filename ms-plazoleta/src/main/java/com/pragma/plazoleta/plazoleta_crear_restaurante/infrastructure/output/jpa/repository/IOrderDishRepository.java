package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderDishEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrderDishRepository extends JpaRepository<OrderDishEntity, OrderDishEntity.OrderDishId> {
}
