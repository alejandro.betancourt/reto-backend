package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Data;

@Data
public class RoleDto {
    private String name;
}
