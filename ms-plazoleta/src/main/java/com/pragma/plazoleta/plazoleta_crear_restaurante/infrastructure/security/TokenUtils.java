package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.security;

import io.jsonwebtoken.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;

public class TokenUtils {

    public static final  String ACCESS_TOKEN_SECRET = "4Q8u39fZdWIWde8qZv5P57TG9WAmSo33";

    private  TokenUtils(){}

    public static UsernamePasswordAuthenticationToken getAuthentication(String token){
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(ACCESS_TOKEN_SECRET.getBytes())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();

            String email = claims.getSubject();
            String roles = (String) claims.get("role");
            List<GrantedAuthority> authorityList = Collections.singletonList(new SimpleGrantedAuthority(roles));

            return new UsernamePasswordAuthenticationToken(email, null, authorityList);
        } catch (JwtException e){
            return null;
        }
    }

}
