package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderDetails;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface OrderRequestMapper {

    OrderRequestMapper INSTANCE = Mappers.getMapper(OrderRequestMapper.class);

    @Mapping(source = "restaurantId", target = "orderModel.restaurantId")
    @Mapping(source = "dishes", target = "dishModel")
    @Mapping(target = "amount", source = "dishes", qualifiedByName = "extractAmounts")
    OrderDishModel toOrder(OrderRequestDto orderRequestDto);

    @Named("extractAmounts")
    default List<Integer> extractAmounts(List<OrderDetails> dishes) {
        return dishes.stream().map(OrderDetails::getAmount).collect(Collectors.toList());
    }
}
