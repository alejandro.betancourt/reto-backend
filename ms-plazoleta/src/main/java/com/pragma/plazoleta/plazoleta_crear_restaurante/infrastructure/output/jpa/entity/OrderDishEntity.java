package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "order_dish")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDishEntity {

    @EmbeddedId
    private OrderDishId orderDishId;

    @ManyToOne
    @MapsId("orderId")
    @JoinColumn(name = "order_id", nullable = false)
    private OrderEntity orderEntity;

    @ManyToOne
    @MapsId("dishId")
    @JoinColumn(name = "dish_id", nullable = false)
    private DishEntity dishEntity;

    @Column(nullable = false)
    private Integer amount;

    @Getter
    @Setter
    @Embeddable
    @EqualsAndHashCode
    @NoArgsConstructor
    @AllArgsConstructor
    public static class OrderDishId implements Serializable {

        @Column(name = "order_id")
        private Long orderId;

        @Column(name = "dishOrder")
        private Long dishId;

    }

}
