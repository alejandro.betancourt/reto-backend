package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class InvalidUserRoleException extends RuntimeException{


    public InvalidUserRoleException(String message) {
        super(message);
    }
}
