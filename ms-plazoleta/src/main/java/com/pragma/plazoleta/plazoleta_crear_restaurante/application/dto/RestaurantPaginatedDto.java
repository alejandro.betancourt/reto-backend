package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantPaginatedDto {

    private String name;
    private String urlLogo;
}
