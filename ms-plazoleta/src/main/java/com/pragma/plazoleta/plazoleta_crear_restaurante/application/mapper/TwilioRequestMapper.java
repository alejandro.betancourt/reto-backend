package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.TwilioRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.TwilioModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)

public interface TwilioRequestMapper {

    TwilioRequestDto toRequest(TwilioModel twilioModel);

}
