package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.configuration;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IDishServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IOrderServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IRestaurantServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.fpi.TwilioFeignClientPort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IDishPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IOrderPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase.DishUseCase;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase.OrderUseCase;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase.RestaurantUseCase;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.DishJpaAdapter;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.OrderJpaAdapter;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.RestaurantJpaAdapter;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.UserFeignClient;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IDishEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IOrderEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IOrderEntityPaginatedMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;
    private final UserFeignClient userFeignClient;
    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;
    private final IRestaurantPaginatedRepository restaurantPaginatedRepository;
    private final IOrderDishRepository orderDishRepository;
    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;
    private final IOrderEntityPaginatedMapper orderEntityPaginatedMapper;
    private final TwilioFeignClientPort twilioFeignClientPort;



    @Bean
    public IRestaurantPersistencePort restaurantPersistencePort() {
        return new RestaurantJpaAdapter(restaurantRepository, restaurantEntityMapper, restaurantPaginatedRepository);
    }

    @Bean
    public IRestaurantServicePort restaurantServicePort() {
        return new RestaurantUseCase(restaurantPersistencePort(), restaurantEntityMapper, userFeignClient);
    }
    @Bean
    public IDishPersistencePort dishPersistencePort() {
        return new DishJpaAdapter(dishRepository, dishEntityMapper);
    }

    @Bean
    public IDishServicePort dishServicePort() {
        return new DishUseCase(dishPersistencePort(), restaurantPersistencePort());
    }

    @Bean
    public IOrderServicePort orderServicePort() {
        return new OrderUseCase(orderPersistencePort(), twilioFeignClientPort, userFeignClient);
    }

    @Bean
    public IOrderPersistencePort orderPersistencePort() {
        return new OrderJpaAdapter(orderRepository, orderDishRepository, orderEntityMapper, orderEntityPaginatedMapper);
    }
}
