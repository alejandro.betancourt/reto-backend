package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;

import java.util.List;

public interface IOrderPersistencePort {

    OrderModel saveOrder(OrderModel orderModel);

    void saveOrderDetails(OrderDishModel orderDishModelList);

    OrderModel getOrder(Long orderId);

    Long getIdByAuthentication();

    List<OrderModel> getOrderByUserId(Long userId);

    List<OrderModel> getAllOrder();

    List<OrderModel> getPaginatedAndSortedRestaurants(int page, int size, String state);

    OrderModel updateStateOrder(Long orderId, OrderModel orderModel);

}
