package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoOrderFoundException extends RuntimeException{

    public NoOrderFoundException(String message){
        super(message);
    }
}
