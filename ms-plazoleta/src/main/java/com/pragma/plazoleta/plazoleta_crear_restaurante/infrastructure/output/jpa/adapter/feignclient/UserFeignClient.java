package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@FeignClient(name = "user-plazoleta", url = "localhost:8090/api/v1/user")
public interface UserFeignClient {

    @GetMapping( "/{id}")
    ResponseEntity<UserDto> getUserById(@PathVariable (name = "id") Long userId);

}
