package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.IRestaurantPaginatedRepository;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.IRestaurantRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@RequiredArgsConstructor
public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private  final IRestaurantEntityMapper restaurantEntityMapper;
    private  final IRestaurantPaginatedRepository restaurantPaginatedRepository;

    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        RestaurantEntity restaurantEntity = restaurantRepository.save(restaurantEntityMapper.toEntity(restaurantModel));
        return restaurantEntityMapper.toRestaurantModel(restaurantEntity);

    }

    @Override
    public RestaurantModel getRestaurant(Long restaurantId) {
        return restaurantEntityMapper.toRestaurantModel(restaurantRepository.findById(restaurantId).orElse(null));
    }


    @Override
    public List<RestaurantModel> getAllRestaurant() {
        List<RestaurantEntity> entityList = restaurantRepository.findAll();
        if (entityList.isEmpty()) {
            throw new NoDataFoundException("Not restaurants found");
        }
        return restaurantEntityMapper.toRestaurantList(entityList);
    }

    @Override
    public List<RestaurantModel> getPaginatedAndSortedRestaurants(int page, int size) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.by("name").ascending());
        Page<RestaurantEntity> restaurantPage = restaurantPaginatedRepository.findAll(pageRequest);
        if (restaurantPage.isEmpty()) {
            throw new NoDataFoundException("No restaurants found on the page");
        }
        return restaurantEntityMapper.toRestaurantList(restaurantPage.getContent());
    }

}
