package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.UserDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IOrderServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.fpi.TwilioFeignClientPort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.TwilioModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IOrderPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoCreateOrderException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoDataFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoOrderFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoUserFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.UserFeignClient;

import java.time.LocalDate;
import java.util.*;

public class OrderUseCase implements IOrderServicePort {

    private final IOrderPersistencePort orderPersistencePort;
    private final TwilioFeignClientPort twilioFeignClientPort;
    private final UserFeignClient userFeignClient;


    public OrderUseCase(IOrderPersistencePort orderPersistencePort, TwilioFeignClientPort twilioFeignClientPort, UserFeignClient userFeignClient) {
        this.orderPersistencePort = orderPersistencePort;
        this.twilioFeignClientPort = twilioFeignClientPort;
        this.userFeignClient = userFeignClient;
    }

    @Override
    public void saveOrder(OrderDishModel orderDishModel) {
        Long userId = orderPersistencePort.getIdByAuthentication();
        List<OrderModel> orderModelDb = orderPersistencePort.getOrderByUserId(userId);
        if (!orderModelDb.isEmpty()) {
            for (OrderModel order :
                    orderModelDb) {
                if (!(order.getState().equalsIgnoreCase("Cancelado") ||
                        order.getState().equalsIgnoreCase("Entregado"))) {
                    throw new NoCreateOrderException("You have an order in process");
                }
            }
        }
        OrderModel orderModel = new OrderModel();
        orderModel.setState("Pendiente");
        orderModel.setDate(LocalDate.now());
        orderModel.setChefId(null);
        orderModel.setUserId(userId);
        orderModel.setRestaurantId(orderDishModel.getOrderModel().getRestaurantId());
        orderModel = orderPersistencePort.saveOrder(orderModel);
        orderDishModel.setOrderModel(orderModel);
        orderPersistencePort.saveOrderDetails(orderDishModel);
    }


    @Override
    public OrderModel getOrder(Long orderId) {
        OrderModel orderModel = orderPersistencePort.getOrder(orderId);
        if (orderModel == null) {
            throw new NoOrderFoundException("Order not found");
        }
        return orderPersistencePort.getOrder(orderId);
    }

    @Override
    public List<OrderModel> getAllOrder() {
        List<OrderModel> list = orderPersistencePort.getAllOrder();
        if (list.isEmpty()) {
            throw new NoDataFoundException("Not dish found");
        }
        return list;
    }

    @Override
    public List<OrderModel> getPaginatedAndSortedRestaurants(int page, int size, String state) {
        List<OrderModel> list = orderPersistencePort.getPaginatedAndSortedRestaurants(page, size, state);
        if (list.isEmpty()) {
            throw new NoDataFoundException("Not order found");
        }
        return list;
    }

    @Override
    public OrderModel updateStateOrder(Long orderId, OrderModel orderModel) {
        OrderModel orderModelDb = orderPersistencePort.getOrder(orderId);
        orderModelDb.setState(orderModel.getState());
        orderModelDb.setChefId(orderModel.getChefId());
        return orderPersistencePort.updateStateOrder(orderId, orderModelDb);
    }

    @Override
    public void updateOrderPreparation(Long orderId) {
        OrderModel orderModelDb = orderPersistencePort.getOrder(orderId);
        if (orderModelDb == null) {
            throw new NoOrderFoundException("Order not found");
        }
        String phoneNumber = getPhoneNumberByFeign(orderModelDb.getUserId());
        String currentState = orderModelDb.getState();

        if ("En_preparacion".equalsIgnoreCase(currentState)) {
            orderModelDb.setState("Listo");

        } else if ("Listo".equalsIgnoreCase(currentState)) {
            twilioFeignClientPort.sendMessage(new TwilioModel("La orden esta lista", phoneNumber));
            orderModelDb.setState("Entregado");
        } else if ("Entregado".equalsIgnoreCase(currentState)) {
            throw new NoOrderFoundException("A delivered order cannot be modified.");
        } else {
            throw new NoOrderFoundException("The order is not ready to be delivered.");
        }

        orderPersistencePort.updateStateOrder(orderId, orderModelDb);
    }

    @Override
    public void cancelOrder(Long orderId) {
        OrderModel orderModelDb = orderPersistencePort.getOrder(orderId);
        if (orderModelDb == null) {
            throw new NoOrderFoundException("Order not found");
        }
        String phoneNumber = getPhoneNumberByFeign(orderModelDb.getUserId());
        String currentState = orderModelDb.getState();

        if(!currentState.equalsIgnoreCase("Pendiente")){
            throw  new NoOrderFoundException("Sorry, your order is already being prepared and cannot be canceled");
        }
        twilioFeignClientPort.sendMessage(new TwilioModel("Order canceled", phoneNumber));
        orderModelDb.setState("Cancelado");
        orderPersistencePort.updateStateOrder(orderId, orderModelDb);

    }


    public String getPhoneNumberByFeign(Long userId){
        UserDto user = userFeignClient.getUserById(userId).getBody();
        if(user == null){
            throw new NoUserFoundException("User not found");
        }
        return user.getCellphone();

    }

}
