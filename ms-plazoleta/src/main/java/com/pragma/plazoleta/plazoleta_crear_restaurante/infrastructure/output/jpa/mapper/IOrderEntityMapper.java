package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper;


import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderDishEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderEntity;

import java.util.List;

public interface IOrderEntityMapper {

    OrderEntity toOrderEntity(OrderModel orderModel);

    OrderModel toOrderModel(OrderEntity orderEntity);

    List<OrderDishEntity> toOrderDishEntity(OrderDishModel orderDishModel);

    List<OrderModel> toOrderModelList(List<OrderEntity> orderEntityList);


}
