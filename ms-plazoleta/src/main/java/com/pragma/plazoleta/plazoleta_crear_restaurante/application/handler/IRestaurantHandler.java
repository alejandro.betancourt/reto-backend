package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantResponseDto;

import java.util.List;

public interface IRestaurantHandler {
    void saveRestaurant(RestaurantRequestDto restaurantRequestDto);

    List<RestaurantPaginatedDto> getPaginatedAndSortedRestaurants(int page, int size);

    List<RestaurantResponseDto> getAllRestaurant();
}
