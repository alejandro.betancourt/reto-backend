package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.impl;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantResponseDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IRestaurantHandler;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.RestaurantRequestMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.RestaurantResponseMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IRestaurantServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class RestaurantHandler implements IRestaurantHandler {

    private final IRestaurantServicePort restaurantServicePort;
    private final RestaurantRequestMapper restaurantRequestMapper;
    private final RestaurantResponseMapper restaurantResponseMapper;

    @Override
    public void saveRestaurant(RestaurantRequestDto restaurantRequestDto) {
        RestaurantModel restaurantModel = restaurantRequestMapper.toRestaurant(restaurantRequestDto);
        restaurantServicePort.saveRestaurant(restaurantModel);
    }

    @Override
    public List<RestaurantPaginatedDto> getPaginatedAndSortedRestaurants(int page, int size) {
        List<RestaurantModel> restaurantModels = restaurantServicePort.getPaginatedAndSortedRestaurants(page, size);
        return restaurantResponseMapper.toResponsePaginatedRestaurant(restaurantModels);
    }

    @Override
    public List<RestaurantResponseDto> getAllRestaurant() {
        return restaurantResponseMapper.toResponseList(restaurantServicePort.getAllRestaurant());
    }
}
