package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderPaginatedDto {

    private Long orderId;
    private Long userId;
    private LocalDate date;
    private String state;
    private Long chefId;
    private Long restaurantId;
}
