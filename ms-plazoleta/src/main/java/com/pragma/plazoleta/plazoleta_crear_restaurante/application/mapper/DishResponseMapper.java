package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishResponseDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface DishResponseMapper {

    DishResponseDto toResponse(DishModel dishModel);

    List<DishResponseDto> toResponseList(List<DishModel> dishModelList);

    List<DishPaginatedDto> toResponsePaginatedDish(List<DishModel> dishModelList);

}
