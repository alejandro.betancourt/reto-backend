package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "restaurant")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RestaurantEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(nullable = false)
    private Long restaurantId;

    @Column
    private String name;

    @Column
    private String nit;

    @Column
    private String address;

    @Column
    private String cellphone;

    @Column
    private String urlLogo;

    @Column
    private Long userId;

}
