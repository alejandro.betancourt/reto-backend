package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantResponseDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RestaurantResponseMapper {

    RestaurantResponseDto toResponse(RestaurantModel restaurantModel);

    List<RestaurantResponseDto> toResponseList(List<RestaurantModel> restaurantModelList);

    List<RestaurantPaginatedDto> toResponsePaginatedRestaurant(List<RestaurantModel> restaurantModelList);
}
