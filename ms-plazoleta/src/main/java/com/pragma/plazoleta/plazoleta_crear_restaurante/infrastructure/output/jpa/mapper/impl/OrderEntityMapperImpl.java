package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.impl;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.DishEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderDishEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IOrderEntityMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Component
public class OrderEntityMapperImpl implements IOrderEntityMapper {


    @Override
    public OrderEntity toOrderEntity(OrderModel orderModel) {
        if (orderModel == null){
            return null;
        }
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(orderModel.getOrderId());
        orderEntity.setDate(orderModel.getDate());
        orderEntity.setState(orderModel.getState());
        orderEntity.setRestaurant(getRestaurantEntityByOrderModel(orderModel));
        orderEntity.setUserId(orderModel.getUserId());
        orderEntity.setChefId(orderModel.getChefId());

        return orderEntity;
    }

    @Override
    public OrderModel toOrderModel(OrderEntity orderEntity) {
        if (orderEntity == null){
            return null;
        }
        OrderModel orderModel = new OrderModel();
        orderModel.setOrderId(orderEntity.getOrderId());
        orderModel.setState(orderEntity.getState());
        orderModel.setDate(orderEntity.getDate());
        orderModel.setChefId(orderEntity.getChefId());
        orderModel.setUserId(orderEntity.getUserId());
        orderModel.setRestaurantId(orderEntity.getRestaurant().getRestaurantId());

        return orderModel;
    }

    @Override
    public List<OrderDishEntity> toOrderDishEntity(OrderDishModel orderDishModel) {
        if (orderDishModel == null){
            return Collections.emptyList();
        }

        List<OrderDishEntity> orderDishEntityList = new ArrayList<>();

        for (int i = 0; i < orderDishModel.getDishModel().size(); i++) {
            OrderDishEntity orderDishEntity = new OrderDishEntity();
            OrderDishEntity.OrderDishId orderDishId = new OrderDishEntity.OrderDishId();
            orderDishId.setDishId(orderDishModel.getDishModel().get(i).getDishId());
            orderDishId.setOrderId(orderDishModel.getOrderModel().getOrderId());
            orderDishEntity.setOrderDishId(orderDishId);
            orderDishEntity.setOrderEntity(toOrderEntity(orderDishModel.getOrderModel()));
            orderDishEntity.setDishEntity(toDishEntityByDishModel(orderDishModel.getDishModel().get(i)));
            orderDishEntity.setAmount(orderDishModel.getAmount().get(i));
            orderDishEntityList.add(orderDishEntity);
        }
        return orderDishEntityList;
    }

    @Override
    public List<OrderModel> toOrderModelList(List<OrderEntity> orderEntityList) {
        if (orderEntityList.isEmpty()){
            return Collections.emptyList();
        }
        List<OrderModel> orderModelList = new ArrayList<>(orderEntityList.size());
        for (OrderEntity orderEntity: orderEntityList) {
            orderModelList.add(toOrderModel(orderEntity));
        }
        return orderModelList;
    }
    


    protected RestaurantEntity getRestaurantEntityByOrderModel(OrderModel orderModel){
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setRestaurantId(orderModel.getRestaurantId());
        return restaurantEntity;
    }
    

    protected DishEntity toDishEntityByDishModel(DishModel dishModel){
        DishEntity dishEntity = new DishEntity();
        dishEntity.setDishId( dishModel.getDishId());
        return dishEntity;
    }
}
