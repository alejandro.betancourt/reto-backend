package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface OrderResponsePaginatedMapper {

    List<OrderPaginatedDto> toResponsePaginatedOrder(List<OrderModel> orderModelList);
}
