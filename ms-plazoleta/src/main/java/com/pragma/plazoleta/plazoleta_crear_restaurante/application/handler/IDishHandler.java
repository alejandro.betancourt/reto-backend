package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.*;

import java.util.List;

public interface IDishHandler {
    void saveDish(DishRequestDto dishRequestDto);

    void updateDish(DishUpdateDto dishUpdateDto);

    void updateStateDish(Long dishId, DishUpdateStateDto dishUpdateStateDto);

    DishResponseDto getDishById(Long dishId);

    List<DishResponseDto> getAllDish();

    List<DishPaginatedDto> getPaginatedAndSortedDishes(int page, int size, String category);

}
