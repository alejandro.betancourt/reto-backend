package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IDishPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.utils.ExtractAuthenticationData;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.DishEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IDishEntityMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository.IDishRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

@RequiredArgsConstructor
public class DishJpaAdapter implements IDishPersistencePort {

    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;

    @Override
    public DishModel saveDish(DishModel dishModel) {
        DishEntity dishEntity = dishRepository.save(dishEntityMapper.toEntity(dishModel));
        return dishEntityMapper.toDishtModel(dishEntity);
    }

    @Override
    public DishModel updateDish(DishModel dishModel) {
        DishEntity dishEntity = dishRepository.save(dishEntityMapper.toEntity(dishModel));
        return dishEntityMapper.toDishtModel(dishEntity);
    }

    @Override
    public DishModel updateStateDish(Long dishId, DishModel dishModel) {
        DishEntity dishEntity = dishRepository.save(dishEntityMapper.toEntity(dishModel));
        return dishEntityMapper.toDishtModel(dishEntity);
    }

    @Override
    public DishModel getDish(Long dishId) {
        return dishEntityMapper.toDishtModel(dishRepository.findById(dishId).orElse(null));
    }

    @Override
    public Long getIdByAuthentication() {
        return ExtractAuthenticationData.getIdByAuthentication();
    }

    @Override
    public List<DishModel> getAllDish() {
        List<DishEntity> entityList = dishRepository.findAll();
        return dishEntityMapper.toDishList(entityList);
    }

    @Override
    public List<DishModel> getPaginatedAndSortedDishes(int page, int size, String category) {
        Pageable pageRequest = PageRequest.of(page, size, Sort.by("name").ascending());
        Page<DishEntity> dishEntityPage = dishRepository.findAllByCategoryContainingIgnoreCase(pageRequest, category);
        return dishEntityMapper.toDishList(dishEntityPage.getContent());
    }
}
