package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDto {

    private Long userId;
    private String name;
    private String lastName;
    private String identificationDocument;
    private String cellphone;
    private LocalDate dateOfBirth;
    private String email;
    private String password;
    private RoleDto role;
}
