package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderRequestDto {

    private Long restaurantId;
    private List<OrderDetails> dishes;
}
