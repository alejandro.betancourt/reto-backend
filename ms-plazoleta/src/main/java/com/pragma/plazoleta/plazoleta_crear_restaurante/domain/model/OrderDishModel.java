package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDishModel {

    private OrderModel orderModel;
    private List<DishModel> dishModel;
    private List<Integer> amount;
}
