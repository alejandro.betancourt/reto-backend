package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.input.rest;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderUpdateStateDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IOrderHandler;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.input.rest.messages.ResponseMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderRestController {

    private final IOrderHandler orderHandler;

    @Operation(summary = "Add a new order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order created", content = @Content),
            @ApiResponse(responseCode = "409", description = "Order already exists", content = @Content)
    })
    @PostMapping()
    @PreAuthorize("hasAuthority('cliente')")
    public ResponseEntity<ResponseMessage> saveOrder(@RequestBody @Valid OrderRequestDto orderRequestDto) {
        orderHandler.saveOrder(orderRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage("Order create"));
    }

    @Operation(summary = "Get paginated orders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Paginated orders returned",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = OrderPaginatedDto.class, type = "Page"))
            ),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/paginated")
    @PreAuthorize("hasAuthority('empleado')")
    public ResponseEntity<List<OrderPaginatedDto>> getPaginatedRestaurant(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "") String state
    ) {
        List<OrderPaginatedDto> orderPaginatedDto = orderHandler.getPaginatedAndSortedOrders(page, size, state);
        return ResponseEntity.ok(orderPaginatedDto);
    }


    @Operation(summary = "Update order status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order updated successfully"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping ("/state/{id}")
    @PreAuthorize("hasAuthority('empleado')")
    public ResponseEntity<String> updateStateOrder(@PathVariable Long id,@RequestBody @Valid OrderUpdateStateDto orderUpdateStateDto) {
        orderHandler.updateStateOrder(id, orderUpdateStateDto);
        return ResponseEntity.ok("Order state updated successfully");
    }

    @Operation(summary = "Update order preparation state")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order updated successfully"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping ("/state/preparation/{id}")
    @PreAuthorize("hasAuthority('empleado')")
    public ResponseEntity<String> updateStatePreparationOrder(@PathVariable Long id) {
        orderHandler.updateOrderPreparation(id);
        return ResponseEntity.ok("Order preparation state updated successfully");
    }

    @Operation(summary = "Cancel order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order canceled successfully"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping ("/state/cancel/{id}")
    @PreAuthorize("hasAuthority('cliente')")
    public ResponseEntity<String> cancelOrder(@PathVariable Long id) {
        orderHandler.cancelOrder(id);
        return ResponseEntity.ok("Order canceled successfully");
    }
}
