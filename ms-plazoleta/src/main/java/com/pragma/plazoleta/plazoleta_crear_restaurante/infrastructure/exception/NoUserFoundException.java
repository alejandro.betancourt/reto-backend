package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoUserFoundException extends RuntimeException{
    public NoUserFoundException(String message){
        super(message);
    }
}
