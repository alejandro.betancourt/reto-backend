package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.impl;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.*;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IDishHandler;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.DishRequestMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.DishResponseMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.DishUpdateMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.DishUpdateStateMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IDishServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@RequiredArgsConstructor
@Transactional
public class DishHandler implements IDishHandler {

    private final IDishServicePort dishServicePort;
    private final DishRequestMapper dishRequestMapper;
    private final DishResponseMapper dishResponseMapper;
    private final DishUpdateMapper dishUpdateMapper;
    private final DishUpdateStateMapper dishUpdateStateMapper;
    @Override
    public void saveDish(DishRequestDto dishRequestDto) {
        DishModel dishModel = dishRequestMapper.toDish(dishRequestDto);
        dishServicePort.saveDish(dishModel);
    }

    @Override
    public void updateDish(DishUpdateDto dishUpdateDto) {
        DishModel dishModel =  dishUpdateMapper.toDish(dishUpdateDto);
        dishServicePort.updateDish(dishModel);
    }

    @Override
    public void updateStateDish(Long dishId, DishUpdateStateDto dishUpdateStateDto) {
        DishModel dishModel = dishUpdateStateMapper.toDish(dishUpdateStateDto);
        dishServicePort.updateStateDish(dishId, dishModel);
    }

    @Override
    public DishResponseDto getDishById(Long dishId) {
        DishModel dishModel = dishServicePort.getDish(dishId);
        return dishRequestMapper.toDto(dishModel);
    }

    @Override
    public List<DishResponseDto> getAllDish() {
        return dishResponseMapper.toResponseList(dishServicePort.getAllDish());
    }

    @Override
    public List<DishPaginatedDto> getPaginatedAndSortedDishes(int page, int size, String category) {
        return dishResponseMapper.toResponsePaginatedDish(dishServicePort.getPaginatedAndSortedDishes(page, size, category));
    }
}
