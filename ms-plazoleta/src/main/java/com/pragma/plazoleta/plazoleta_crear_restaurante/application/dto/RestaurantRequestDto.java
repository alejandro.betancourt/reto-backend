package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
public class RestaurantRequestDto {

    @NotBlank(message = "Name is required")
    @Pattern(regexp = "^(?!\\d+$).+", message = "Name must not consist only of numbers")
    private String name;

    @NotBlank(message = "Nit name is required")
    @Pattern(regexp = "[\\d]*", message = "Nit must be numeric")
    private String nit;

    @NotBlank(message = "Address is required")
    private String address;

    @NotBlank(message = "Cellphone is required")
    @Size(max = 13, message = "The phone number must contain a maximum of 13 characters")
    @Pattern(regexp = "[+|\\d][\\d]*", message = "fields must be numeric")
    private String cellphone;

    @NotBlank(message = "UrlLogo is required")
    private String urlLogo;

    private Long userId;

}
