package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import static com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.security.TokenUtils.ACCESS_TOKEN_SECRET;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

public class ExtractAuthenticationData {

    private static final String BEARER_CONSTANT = "Bearer ";

     private ExtractAuthenticationData(){}

     private static String getToken(){
         ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
         if(requestAttributes == null){
             throw new NullPointerException();
         }
         HttpServletRequest request = requestAttributes.getRequest();
         String bearerToken = request.getHeader(AUTHORIZATION);
         if (bearerToken != null && bearerToken.startsWith(BEARER_CONSTANT)){
             return bearerToken.replace(BEARER_CONSTANT, "");
         }
         return null;
     }

    public static Long getIdByAuthentication(){
         String token = getToken();
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(ACCESS_TOKEN_SECRET.getBytes())
                .build()
                .parseClaimsJws(token)
                .getBody();

        return claims.get("id", Long.class);
    }

}

