package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "dish")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishEntity {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(nullable = false)
    private Long dishId;

    @Column
    private String name;

    @Column
    private Integer price;

    @Column
    private String description;

    @Column
    private String urlImage;

    @Column
    private String category;

    @Column
    private Boolean state;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    private RestaurantEntity restaurant;

}
