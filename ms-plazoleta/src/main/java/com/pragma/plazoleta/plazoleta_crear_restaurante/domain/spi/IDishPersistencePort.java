package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;

import java.util.List;

public interface IDishPersistencePort {

    DishModel saveDish(DishModel dishModel);

    DishModel updateDish(DishModel dishModel);

    DishModel updateStateDish(Long dishId, DishModel dishModel);

    DishModel getDish(Long dishId);

    Long getIdByAuthentication();

    List<DishModel> getAllDish();

    List<DishModel> getPaginatedAndSortedDishes(int page, int size, String category);

}
