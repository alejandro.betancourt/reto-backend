package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IOrderRepository extends JpaRepository<OrderEntity, Long> {

    Page<OrderEntity> findAllByStateContainingIgnoreCase(Pageable pageable, String state);

    List<OrderEntity> findOrderEntityByUserId(Long userId);
}
