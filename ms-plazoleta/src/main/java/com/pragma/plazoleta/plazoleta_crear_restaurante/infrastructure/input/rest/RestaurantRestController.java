package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.input.rest;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.RestaurantResponseDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IRestaurantHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/restaurant")
@RequiredArgsConstructor
public class RestaurantRestController {

    private  final IRestaurantHandler restaurantHandler;

    @Operation(summary = "Add a new restaurant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Restaurant created", content = @Content),
            @ApiResponse(responseCode = "409", description = "Restaurant already exists", content = @Content)
    })
    @PostMapping()
    @PreAuthorize("hasAuthority('administrador')")
    public ResponseEntity<Void> saveRestaurant(@RequestBody @Valid RestaurantRequestDto restaurantRequestDto) {
        restaurantHandler.saveRestaurant(restaurantRequestDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Get all restaurants")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All restaurants returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = RestaurantResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping()
    @PreAuthorize("hasAuthority('administrador')")
    public ResponseEntity<List<RestaurantResponseDto>> getAllRestaurant() {
        return ResponseEntity.ok(restaurantHandler.getAllRestaurant());
    }

    @Operation(summary = "Get paginated restaurants")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Paginated restaurants returned",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Page.class, type = "Page"))
            ),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/paginated")
    @PreAuthorize("hasAuthority('cliente')")
    public ResponseEntity<List<RestaurantPaginatedDto>> getPaginatedRestaurant(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        List<RestaurantPaginatedDto> paginatedRestaurants = restaurantHandler.getPaginatedAndSortedRestaurants(page, size);
        return ResponseEntity.ok(paginatedRestaurants);
    }
}
