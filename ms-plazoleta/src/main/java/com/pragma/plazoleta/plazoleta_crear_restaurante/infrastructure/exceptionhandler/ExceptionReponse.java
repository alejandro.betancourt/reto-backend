package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exceptionhandler;

public enum ExceptionReponse {

    NO_DATA_FOUND("No data found for the requested petition");

    private final String message;

    ExceptionReponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
