package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class DishRequestDto {

    @NotBlank(message = "Name is required")
    private String name;

    @NotNull(message = "Price is required")
    @Positive(message = "Price must be a positive number")
    @Min(value = 1, message = "Price must be greater than 0")
    private Integer price;

    @NotBlank(message = "description is required")
    private String description;

    @NotBlank(message = "urlImage is required")
    private String urlImage;

    @NotBlank(message = "category is required")
    private String category;

    @NotNull(message = "restaurantId is required")
    private Long restaurantId;

}
