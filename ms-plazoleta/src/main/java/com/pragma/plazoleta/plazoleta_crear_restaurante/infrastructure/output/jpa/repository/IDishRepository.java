package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.repository;

import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.DishEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDishRepository extends JpaRepository<DishEntity, Long> {

    Page<DishEntity> findAllByCategoryContainingIgnoreCase(Pageable pageable, String category);
}
