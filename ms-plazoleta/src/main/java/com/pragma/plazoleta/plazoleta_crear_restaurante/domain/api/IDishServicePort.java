package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import java.util.List;

public interface IDishServicePort {

    DishModel saveDish(DishModel dishModel);

    DishModel updateDish(DishModel dishModel);

    DishModel updateStateDish(Long dishId, DishModel dishModel);

    DishModel getDish(Long dishId);

    List<DishModel> getAllDish();

    List<DishModel> getPaginatedAndSortedDishes(int page, int size, String category);
}
