package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException(String message){
        super(message);
    }
}
