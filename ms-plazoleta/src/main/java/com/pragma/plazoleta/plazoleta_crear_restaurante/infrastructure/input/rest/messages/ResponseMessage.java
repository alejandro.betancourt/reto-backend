package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.input.rest.messages;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ResponseMessage {

    private String message;
}
