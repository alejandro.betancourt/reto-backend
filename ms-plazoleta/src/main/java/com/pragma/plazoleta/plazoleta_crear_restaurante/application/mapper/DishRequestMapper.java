package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishResponseDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface DishRequestMapper {
    DishModel toDish(DishRequestDto dishRequestDto);

    DishResponseDto toDto(DishModel dishModel);
}
