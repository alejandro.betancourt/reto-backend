package com.pragma.plazoleta.plazoleta_crear_restaurante;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PlazoletaCrearRestauranteApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlazoletaCrearRestauranteApplication.class, args);
	}

}
