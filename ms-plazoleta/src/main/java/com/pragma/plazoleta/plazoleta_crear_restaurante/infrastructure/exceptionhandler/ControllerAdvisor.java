package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exceptionhandler;

import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.*;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ControllerAdvisor {

    private static final String MESSAGE = "message";

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException noDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, noDataFoundException.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, List<String>>> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toList());

        Map<String, List<String>> response = Collections.singletonMap(MESSAGE, errorMessages);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(NoUserFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoUserFoundException(
            NoUserFoundException noUserFoundException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, noUserFoundException.getMessage()));
    }

    @ExceptionHandler(InvalidUserRoleException.class)
    public ResponseEntity<Map<String, String>> handleInvalidUserRoleException(
            InvalidUserRoleException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(NoRestaurantFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoRestaurantFoundException(
            NoRestaurantFoundException noRestaurantFoundException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, noRestaurantFoundException.getMessage()));
    }

    @ExceptionHandler(NoDishFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDishFoundException(
            NoDishFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Map<String, String>> handleAccessDeniedException(
            AccessDeniedException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(RestaurantNoBelongOwnerException.class)
    public ResponseEntity<Map<String, String>> handleRestaurantNoBelongOwnerException(
            RestaurantNoBelongOwnerException ex) {
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(RestaurantNoBelongEmployeeException.class)
    public ResponseEntity<Map<String, String>> handleNoOrderFoundException(
            RestaurantNoBelongEmployeeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(NoOrderFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoOrderFoundException(
            NoOrderFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(NoCreateOrderException.class)
    public ResponseEntity<Map<String, String>> handleNoCreateFoundException(
            NoCreateOrderException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

    @ExceptionHandler(TwilioFeignClientException.class)
    public ResponseEntity<Map<String, String>> handleTwilioFeignClientException(
            TwilioFeignClientException ex) {
        return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT)
                .body(Collections.singletonMap(MESSAGE, ex.getMessage()));
    }

//    @ExceptionHandler(FeignException.class)
//    public ResponseEntity<Map<String, String>> handleFeignException(
//            FeignException feignException) throws InterruptedException {
//        int status = HttpStatus.REQUEST_TIMEOUT.value();
//        String message = "An error occurred while trying to consume a service";
//        if (feignException.status() > 100) {
//            status = feignException.status();
//            message = feignException.contentUTF8();
//        }
//        Thread.sleep(1000);
//        return ResponseEntity.status(status)
//                .body(Collections.singletonMap("Error", message));
//    }


}
