package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishResponseDto {

    private Long dishId;
    private String name;
    private Integer price;
    private String description;
    private String urlImage;
    private String category;
    private Boolean state;
    private Long restaurantId;
}
