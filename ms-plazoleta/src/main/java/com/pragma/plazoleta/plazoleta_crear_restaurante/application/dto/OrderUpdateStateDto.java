package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class OrderUpdateStateDto {

    private Long orderId;

    private Long chefId;

    @NotNull(message = "State is required")
    private String state;

}
