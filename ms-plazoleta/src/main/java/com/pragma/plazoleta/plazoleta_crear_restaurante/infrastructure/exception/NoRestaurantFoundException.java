package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoRestaurantFoundException extends RuntimeException{

    public NoRestaurantFoundException(String message){
        super(message);
    }
}
