package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderUpdateStateDto;

import java.util.List;

public interface IOrderHandler {

    void saveOrder(OrderRequestDto orderRequestDto);

    List<OrderPaginatedDto> getPaginatedAndSortedOrders(int page, int size, String state);

    void updateStateOrder(Long orderId, OrderUpdateStateDto orderUpdateStateDto);

    void updateOrderPreparation(Long orderId);

    void cancelOrder(Long orderId);

}
