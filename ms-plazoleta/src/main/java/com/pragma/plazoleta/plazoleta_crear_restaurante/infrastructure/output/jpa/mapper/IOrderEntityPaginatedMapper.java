package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IOrderEntityPaginatedMapper {

    List<OrderModel> toOrderList(List<OrderEntity> orderEntityList);
}
