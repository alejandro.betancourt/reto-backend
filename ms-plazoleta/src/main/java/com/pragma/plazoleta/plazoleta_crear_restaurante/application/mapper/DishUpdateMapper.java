package com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.DishUpdateDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface DishUpdateMapper {

    DishModel toDish(DishUpdateDto dishUpdateDto);
    DishUpdateDto toUpdateDto(DishModel dishModel);
}
