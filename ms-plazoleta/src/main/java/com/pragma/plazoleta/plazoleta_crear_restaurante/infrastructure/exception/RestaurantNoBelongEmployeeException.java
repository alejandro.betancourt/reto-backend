package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class RestaurantNoBelongEmployeeException extends RuntimeException{

    public RestaurantNoBelongEmployeeException(String message){
        super(message);
    }
}
