package com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class DishUpdateDto {

    @NotNull(message = "Dish ID is required")
    private Long dishId;

    @NotNull(message = "Price is required")
    @Positive(message = "Price must be a positive number")
    @Min(value = 1, message = "Price must be greater than 0")
    private Integer price;

    @NotBlank(message = "description is required")
    private String description;

}
