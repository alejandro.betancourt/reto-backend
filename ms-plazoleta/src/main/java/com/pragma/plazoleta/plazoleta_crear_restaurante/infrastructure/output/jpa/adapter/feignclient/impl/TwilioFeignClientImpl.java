package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.impl;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.TwilioRequestMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.fpi.TwilioFeignClientPort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.TwilioModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.TwilioFeignClientException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.TwilioFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class TwilioFeignClientImpl implements TwilioFeignClientPort {

    private final TwilioFeignClient twilioFeignClient;
    private final TwilioRequestMapper twilioRequestMapper;

    @Override
    public void sendMessage(TwilioModel twilioModel) {
        ResponseEntity<Map<String, String>> responseEntity =
                twilioFeignClient.sendMessage(
                        twilioRequestMapper.toRequest(twilioModel)
                );
        if (!responseEntity.getStatusCode().is2xxSuccessful()) {
            throw new TwilioFeignClientException("An error occurred with the Twilio service");
        }
    }
}
