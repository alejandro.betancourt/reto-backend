package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;

import java.util.List;

public interface IRestaurantPersistencePort {

    RestaurantModel saveRestaurant(RestaurantModel restaurantModel);

    RestaurantModel getRestaurant(Long restaurantId);

    List<RestaurantModel> getAllRestaurant();

    List<RestaurantModel> getPaginatedAndSortedRestaurants(int page, int size);
}
