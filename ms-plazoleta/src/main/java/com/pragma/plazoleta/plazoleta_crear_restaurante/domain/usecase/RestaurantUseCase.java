package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.usecase;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.UserDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IRestaurantServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.spi.IRestaurantPersistencePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.InvalidUserRoleException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception.NoUserFoundException;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.adapter.feignclient.UserFeignClient;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper.IRestaurantEntityMapper;
import feign.FeignException;

import java.util.List;

public class RestaurantUseCase implements IRestaurantServicePort {

    private final IRestaurantPersistencePort restaurantPersistencePort;
    private  final IRestaurantEntityMapper restaurantEntityMapper;
    private final UserFeignClient userFeignClient;

    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort, IRestaurantEntityMapper restaurantEntityMapper, UserFeignClient userFeignClient) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.restaurantEntityMapper = restaurantEntityMapper;
        this.userFeignClient = userFeignClient;
    }


    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        if (restaurantModel.getUserId() == null) {
            throw new NoUserFoundException("User not found");
        }
        try {
            UserDto user = userFeignClient.getUserById(restaurantModel.getUserId()).getBody();
            if(user.getRole().getName().equals("propietario")){
                RestaurantEntity restaurantEntity = restaurantEntityMapper.toEntity(restaurantModel);
                RestaurantModel restaurantM = restaurantEntityMapper.toRestaurantModel(restaurantEntity);
                return restaurantPersistencePort.saveRestaurant(restaurantM);
            } else {
                throw new InvalidUserRoleException("User does not have the required role");
            }
        } catch (FeignException ex) {
            throw new NoUserFoundException("User feign not found");
        }

    }

    @Override
    public RestaurantModel getRestaurant(Long restaurantId) {
        return restaurantPersistencePort.getRestaurant(restaurantId);
    }

    @Override
    public List<RestaurantModel> getAllRestaurant() {
        return restaurantPersistencePort.getAllRestaurant();
    }

    @Override
    public List<RestaurantModel> getPaginatedAndSortedRestaurants(int page, int size) {
        return restaurantPersistencePort.getPaginatedAndSortedRestaurants(page, size);
    }

}
