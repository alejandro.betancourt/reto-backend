package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.fpi;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.TwilioModel;

public interface TwilioFeignClientPort {

    void sendMessage(TwilioModel twilioModel);
}
