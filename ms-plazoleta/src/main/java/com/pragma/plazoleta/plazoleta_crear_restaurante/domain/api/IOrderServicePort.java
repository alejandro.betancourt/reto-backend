package com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderDishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;

import java.util.List;

public interface IOrderServicePort {

    void saveOrder(OrderDishModel orderDishModel);

    OrderModel getOrder(Long orderId);

    List<OrderModel> getAllOrder();

    List<OrderModel> getPaginatedAndSortedRestaurants(int page, int size, String state);

    OrderModel updateStateOrder(Long orderId, OrderModel orderModel);

    void updateOrderPreparation(Long orderId);

    void cancelOrder(Long orderId);
}
