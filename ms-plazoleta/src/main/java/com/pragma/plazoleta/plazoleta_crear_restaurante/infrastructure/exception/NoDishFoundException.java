package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.exception;

public class NoDishFoundException extends RuntimeException{

    public NoDishFoundException(String message){
        super(message);
    }
}
