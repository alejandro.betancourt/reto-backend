package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.RestaurantModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.RestaurantEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IRestaurantEntityMapper {

    RestaurantEntity toEntity(RestaurantModel restaurantModel);
    RestaurantModel toRestaurantModel(RestaurantEntity restaurantEntity);
    List<RestaurantModel> toRestaurantList(List<RestaurantEntity> restaurantEntityList);

}
