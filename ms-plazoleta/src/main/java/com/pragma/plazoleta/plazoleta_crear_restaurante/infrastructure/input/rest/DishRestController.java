package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.input.rest;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.*;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IDishHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/dish")
@RequiredArgsConstructor
public class DishRestController {

    private final IDishHandler dishHandler;

    @Operation(summary = "Add a new dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Dish created", content = @Content),
            @ApiResponse(responseCode = "409", description = "Dish already exists", content = @Content)
    })
    @PostMapping()
    @PreAuthorize("hasAuthority('propietario')")
    public ResponseEntity<Void> dishRestaurant(@RequestBody @Valid DishRequestDto dishRequestDto) {
        dishHandler.saveDish(dishRequestDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Update a dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dish updated successfully"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Dish not found", content = @Content)
    })
    @PatchMapping ()
    @PreAuthorize("hasAuthority('propietario')")
    public ResponseEntity<String> updateDish(@RequestBody @Valid DishUpdateDto dishUpdateDto) {
        dishHandler.updateDish(dishUpdateDto);
        return ResponseEntity.ok("Dish updated successfully");
    }

    @PatchMapping ("/state/{id}")
    @PreAuthorize("hasAuthority('propietario')")
    public ResponseEntity<String> updateStateDish(@PathVariable Long id,@RequestBody @Valid DishUpdateStateDto dishUpdateStateDto) {
        dishHandler.updateStateDish(id, dishUpdateStateDto);
        return ResponseEntity.ok("Dish state updated successfully");
    }

    @GetMapping("/{id}")
    public ResponseEntity<DishResponseDto> getDishById(@PathVariable (name = "id") Long dishId) {
        return ResponseEntity.ok(dishHandler.getDishById(dishId));
    }


    @Operation(summary = "Get all dishes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All dishes returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = DishResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping()
    @PreAuthorize("hasAuthority('administrador')")
    public ResponseEntity<List<DishResponseDto>> getAllDishes() {
        return ResponseEntity.ok(dishHandler.getAllDish());
    }

    @Operation(summary = "Get paginated dishes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Paginated dishes returned",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = DishPaginatedDto.class, type = "Page"))
            ),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/paginated")
    @PreAuthorize("hasAuthority('cliente')")
    public ResponseEntity<List<DishPaginatedDto>> getPaginatedRestaurant(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(defaultValue = "") String category
    ) {
        List<DishPaginatedDto> dishPaginatedDto = dishHandler.getPaginatedAndSortedDishes(page, size, category);
        return ResponseEntity.ok(dishPaginatedDto);
    }
}
