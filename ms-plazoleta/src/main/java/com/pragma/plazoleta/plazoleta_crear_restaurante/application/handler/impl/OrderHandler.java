package com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.impl;

import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderPaginatedDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderRequestDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.dto.OrderUpdateStateDto;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.handler.IOrderHandler;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.OrderRequestMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.OrderResponsePaginatedMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.application.mapper.OrderUpdateStateMapper;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.api.IOrderServicePort;
import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.OrderModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderHandler implements IOrderHandler {

    private final IOrderServicePort orderServicePort;
    private final OrderRequestMapper orderRequestMapper;
    private final OrderResponsePaginatedMapper orderResponsePaginatedMapper;
    private final OrderUpdateStateMapper orderUpdateStateMapper;

    @Override
    public void saveOrder(OrderRequestDto orderRequestDto) {
        orderServicePort.saveOrder(orderRequestMapper.toOrder(orderRequestDto));
    }

    @Override
    public List<OrderPaginatedDto> getPaginatedAndSortedOrders(int page, int size, String state) {
        return orderResponsePaginatedMapper.toResponsePaginatedOrder(orderServicePort.getPaginatedAndSortedRestaurants(page, size, state));
    }

    @Override
    public void updateStateOrder(Long orderId, OrderUpdateStateDto orderUpdateStateDto) {
        OrderModel orderModel = orderUpdateStateMapper.toOrder(orderUpdateStateDto);
        orderServicePort.updateStateOrder(orderId, orderModel);
    }

    @Override
    public void updateOrderPreparation(Long orderId) {
        orderServicePort.updateOrderPreparation(orderId);
    }

    @Override
    public void cancelOrder(Long orderId) {
        orderServicePort.cancelOrder(orderId);
    }
}
