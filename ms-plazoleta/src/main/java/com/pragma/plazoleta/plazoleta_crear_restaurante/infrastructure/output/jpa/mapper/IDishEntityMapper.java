package com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.mapper;

import com.pragma.plazoleta.plazoleta_crear_restaurante.domain.model.DishModel;
import com.pragma.plazoleta.plazoleta_crear_restaurante.infrastructure.output.jpa.entity.DishEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface IDishEntityMapper {

    @Mapping(target = "restaurant.restaurantId", source = "restaurantId")
    DishEntity toEntity(DishModel dishModel);

    @Mapping(target = "restaurantId", source = "restaurant.restaurantId")
    DishModel toDishtModel(DishEntity dishEntity);

    List<DishModel> toDishList(List<DishEntity> dishEntityList);
}
